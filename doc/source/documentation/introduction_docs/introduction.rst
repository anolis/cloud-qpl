 .. ***************************************************************************
 .. * Copyright (C) 2022 Intel Corporation
 .. *
 .. * SPDX-License-Identifier: MIT
 .. ***************************************************************************/


.. _introduction_reference_link:

Introduction
############


The Cloud Query Processing Library (Cloud QPL) can be used to improve
performance of database, enterprise data, communications, and
scientific/technical applications. Cloud QPL provides interfaces for a
number of commonly used algorithms. Using this library enables you to
automatically tune your application to many generations of processors
without changing your application. Cloud QPL provides high
performance implementations of data processing functions for existing
hardware accelerator, and/or software path if the hardware
accelerator is not available. Code written with the library
automatically takes advantage of available modern CPU capabilities. This
can provide tremendous development and maintenance savings. The goal of
Cloud QPL is to provide application programming interface (API)
with:

-  C and C++ compatible interfaces and data structures to enhance usability and portability
-  Faster time to market
-  Scalability with Intel® In-Memory Analytics Accelerator (Intel® IAA) hardware


Library Overview
****************


Cloud Query Processing Library (Cloud QPL) consists of two main
functional blocks: analytics and compression.

The analytics part contains two sub-blocks: Decompress and Filter.
These functions are tied together, so that each analytics operation
can perform decompress-only, filter-only, or decompress-and-filter
processing, as illustrated in the figure below.

Alternatively, you can compress the input with the compression part.

::


                          Decompress               SQL Filter   Decompress
                             Bypass                   Bypass       Output
                      /------------------\   |\   /--------------------\
                      | +--------------+ |   | \  |  +-------------+   |     |\
           Source1    | |   DEFLATE    | \-->|  |-+->| SQL Filter  |   \---->| \
           -----------+-| Decompressor |---->| /     |  Functions  |-------->|  |------------>
                      | +--------------+     |/      +-------------+         | /   Analytics
                      | Decompress   |                      |                |/    Engine Output
           Source2    | Config/State |                      |           SQL Filter
           -----------(--------------(----------------------/             Output
                      |  Compress    |             Filter Optional
                      | Config/State |             Second Input
                      |              |
                      | +--------------+
                      | |   DEFLATE    |
                      +-| Compressor   |----------------------------------------------------->
                        +--------------+

                       Cloud Query Processing Library (Cloud QPL) pipeline


With the library, you can store columnar databases in a compressed form,
decreasing memory footprint. In addition to increased effective memory
capacity, this also reduces memory bandwidth by executing the filter
function used for database queries “on the fly”, avoiding use of memory
bandwidth for uncompressed raw data transfer.

Cloud QPL supports decompression compatible with the Deflate compression
standard described in RFC 1951. The uncompressed data may be written
directly to memory or passed to the input of the filter function.
Decompression is supported for Deflate streams where the size of the
history buffer is no more than 4 KB.

The library also supports Deflate compression, along with the
calculation of arbitrary CRCs.

The SQL filter function block takes one or two input streams, a primary
input, and an optional secondary input. The primary input may be read
from memory or received from the decompression block. The second input,
if used, is always read from memory. The data streams logically contain
an array of unsigned values, but they may be formatted in any of several
ways, e.g., as a packed array. If the bit-width of the values is 1, the
stream will be referenced as a “bit-vector”, otherwise, it will be
referenced as an “array”.

The output of the filter function may be either an array or a bit
vector, depending on the function.

In addition to generating output data, Cloud QPL computes a 32-bit CRC
of the uncompressed data (either the result of decompression, or the
direct input to the filter function), the XOR checksum of this data, and
several “aggregates” of the output data. The CRC, XOR checksum, and
aggregates are written to the completion record.


Intel® In-Memory Analytics Accelerator (Intel® IAA)
===================================================


The Cloud QPL library uses Intel IAA hardware that provides
compression and decompression of very high throughput combined
with analytic primitive functions. The primitive functions are
commonly used for data filtering during analytic query processing.

Intel IAA primarily targets:

-  Big data applications and in-memory analytic databases.
-  Application-transparent usages such as memory page compression.
-  Data integrity operations, e.g., CRC-64.

Intel IAA supports Huffman encoding and
Deflate. For the Deflate format, Intel IAA supports indexing of the
compressed stream for efficient random access.


Library Features
****************

Operations
==========

Cloud QPL supports:

- Deflate compression/decompression with the history size limited to 4 KB
- Huffman-only compression/decompression
- Filter operations

.. warning::
   The implementation of Huffman-only compression/decompression is in progress.

Execution Paths
===============

Cloud QPL supports several execution paths that help to achieve the optimal
system resources utilization:

- ``Hardware Path`` - all hardware-supported functions are executed by Intel IAA.
- ``Software Path`` - all supported functionalities are executed by the software library in the CPU.
- ``Auto Path`` - Cloud QPL automatically dispatches execution of the
  requested operations either to Intel IAA or to the software
  library depending on internal heuristics (``Load Balancing`` feature).

.. warning::
   The implementation of ``Auto Path`` is in progress.


NUMA Support
============

Cloud QPL is NUMA aware and respects the NUMA node ID of the calling
thread. If a user needs to use a device from a specific node, it can be
done in two ways:

-  Pin thread that performs submissions to the specific NUMA, the
   library will use devices only from this node.
-  Set NUMA ID parameter of the job to the specific node ID, then
   devices will be selected only from this node.

Load balancer of the library does not cross a detected or specified NUMA
boundary. Users are responsible for balancing workloads between different nodes.


Work Queues Support
===================

Cloud QPL library uses Intel IAA hardware accelerator that works in Shared Work Queues (SWQ) and Dedicated Work Queues (DWQ). 
The library will automatically detect the WQ type of queues in specific device. Depending on the WQ type, the 
software may use either the ``ENQCMD`` (SWQ) or ``MOVDIR64B`` (DWQ) instruction for descriptor submission.

.. warning::
   The implementation of async mode for DWQ is in progress.


Non-SVM Support
===============

Cloud QPL library is able to work in environments without share virtual memory (SVM). Users may use the command to check if 
the system has SVM support.
```
grep -nr "intel_iommu=on,sm_on" /proc/cmdline
```
If the command has no return, the system does not have IOMMU and does not support SVM. In this case Cloud QPL will use Non-SVM 
solution for descriptor processing. Cloud QPL will not use the Non-SVM solution if SVM is enabled.

.. _library_limitations_reference_link:

Library Limitations
*******************

- Library does not have APIs for the hardware path configuration.
- Library does not have APIs for ``Load Balancing`` feature customization.
- Library does not support hardware path on Windows OS.
- Library is not developed for kernel mode usage. It is user level driver library.

Library APIs
************

Cloud QPL provides Low-Level C API, that represents a state-based interface.
The base idea is to allocate a single state and configure one with different ways
to perform necessary operation. All memory allocations are happening on user side
or via user-provided allocators.
See :ref:`developer_guide_low_level_reference_link` for more details.

