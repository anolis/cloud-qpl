 .. ***************************************************************************
 .. * Copyright (C) 2022 Intel Corporation
 .. *
 .. * SPDX-License-Identifier: MIT
 .. ***************************************************************************/


Status Codes
############

Categories and Ranges of Status Codes
*************************************

.. doxygengroup:: QPL_STATUS_BASE
   :project: Cloud Query Processing Library
   :content-only:

Internal Calculators for Status Codes
*************************************

.. doxygengroup:: QPL_STATUS_CALCULATOR
   :project: Cloud Query Processing Library
   :content-only:

Complete List of Status Codes
*****************************

.. doxygenenum:: qpl_status
    :project: Cloud Query Processing Library
