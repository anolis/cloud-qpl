 .. ***************************************************************************
 .. * Copyright (C) 2022 Intel Corporation
 .. *
 .. * SPDX-License-Identifier: MIT
 .. ***************************************************************************/

Job APIs
########

Functions
*********

.. doxygenfunction:: qpl_get_job_size
   :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_init_job
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_submit_job
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_check_job
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_wait_job
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_execute_job
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_fini_job
    :project: Cloud Query Processing Library


Structures
**********

.. doxygenstruct:: qpl_job
   :project: Cloud Query Processing Library
   :members:
