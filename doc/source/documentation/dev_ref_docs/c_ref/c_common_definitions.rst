 .. ***************************************************************************
 .. * Copyright (C) 2022 Intel Corporation
 .. *
 .. * SPDX-License-Identifier: MIT
 .. ***************************************************************************/

Common Definitions
###########################

Flags
*****

.. doxygengroup:: QPL_FLAGS
   :project: Cloud Query Processing Library
   :content-only:

Enums
*****

.. doxygenenum:: qpl_path_t
   :project: Cloud Query Processing Library
   :outline:

.. doxygenenum:: qpl_operation
   :project: Cloud Query Processing Library
   :outline:

.. doxygenenum:: qpl_compression_levels
   :project: Cloud Query Processing Library

.. doxygenenum:: qpl_statistics_mode
   :project: Cloud Query Processing Library

.. doxygenenum:: qpl_mini_block_size
   :project: Cloud Query Processing Library
   :outline:

.. doxygenenum:: qpl_out_format
   :project: Cloud Query Processing Library

.. doxygenenum:: qpl_parser
   :project: Cloud Query Processing Library

Structures
**********

.. doxygenstruct:: allocator_t
   :project: Cloud Query Processing Library
   :members:

