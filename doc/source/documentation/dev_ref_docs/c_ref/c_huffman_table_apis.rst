 .. ***************************************************************************
 .. * Copyright (C) 2022 Intel Corporation
 .. *
 .. * SPDX-License-Identifier: MIT
 .. ***************************************************************************/

Huffman Table APIs
##################

Functions
*********

Creation and Destruction of Huffman table
-----------------------------------------

.. doxygenfunction:: qpl_deflate_huffman_table_create
   :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_only_table_create
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_destroy
    :project: Cloud Query Processing Library

Initialization of Huffman table
-------------------------------

.. doxygenfunction:: qpl_huffman_table_init_with_triplets
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_gather_deflate_statistics
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_init_with_histogram
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_init_with_other
    :project: Cloud Query Processing Library

Serialization APIs
------------------

.. doxygenfunction:: qpl_huffman_table_get_serialized_size
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_serialize
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_deserialize
    :project: Cloud Query Processing Library

.. doxygenfunction:: qpl_huffman_table_get_type
    :project: Cloud Query Processing Library

Types
*****

.. doxygentypedef:: qpl_huffman_table_t
   :project: Cloud Query Processing Library

Enums
*****

.. doxygenenum:: qpl_huffman_table_type_e
   :project: Cloud Query Processing Library

.. doxygenenum:: qpl_serialization_format_e
   :project: Cloud Query Processing Library

Structures
**********

.. doxygenstruct:: qpl_huffman_triplet
   :project: Cloud Query Processing Library
   :members:

.. doxygenstruct:: qpl_histogram
   :project: Cloud Query Processing Library
   :members:

.. doxygenstruct:: serialization_options_t
   :project: Cloud Query Processing Library
   :members:
