#!/bin/bash

mkdir QAT
tar zxvf QAT.L.4.19.0-00005.tar.gz -C QAT

#install dependencies
yum install -y libudev-devel yasm

cd QAT
./configure
make -j32
make install