/*******************************************************************************
 * Copyright (C) 2022 Intel Corporation
 *
 * SPDX-License-Identifier: MIT
 ******************************************************************************/

#ifndef QPL_DESCRIPTOR_REFILL_HPP
#define QPL_DESCRIPTOR_REFILL_HPP

#include <cstring>
#include <cstdio>
#include "usdm/usdm_mem_api.h"

namespace qpl::ml::util
{

static inline void refill_work_descriptor(const usdm_mem_unit *addr_unit, void *desc)
{
    uint64_t *wd;

    wd = (uint64_t *)desc;

    if (wd[SRC1_ADDR_FIELD] != 0)
    {
        wd[SRC1_ADDR_FIELD] = addr_unit->src1_phys;
    }

    if (wd[SRC2_ADDR_FIELD] != 0)
    {
        wd[SRC2_ADDR_FIELD] = addr_unit->src2_phys;
    }

    if (wd[DEST_ADDR_FIELD] != 0)
    {
        wd[DEST_ADDR_FIELD] = addr_unit->dest_phys;
    }

    if (wd[COMP_ADDR_FIELD] != 0)
    {
        wd[COMP_ADDR_FIELD] = addr_unit->comp_phys;
    }
}

static int has_aecs_flag(const uint64_t *desc, uint8_t *ts)
{
    uint32_t op_flags;
    uint8_t wr_src2;
    uint8_t rd_src2;
#define OP_READ_SOURCE_2_OFT (16)
#define OP_WRITE_SOURCE_2_OFT (18)
#define OP_AECS_RW_TS_OFT (22)

    op_flags = (desc[0] >> 32) & 0xFFFFFFU;
    wr_src2 = (op_flags >> OP_WRITE_SOURCE_2_OFT) & 0x3U;
    rd_src2 = (op_flags >> OP_READ_SOURCE_2_OFT) & 0x3U;

    if (rd_src2 != 1 && wr_src2 == 0)
    {
        return 0;
    }

    *ts = (op_flags >> OP_AECS_RW_TS_OFT) & 0x1U;
    return 1;
}

inline int pre_copy(void *desc_remapping, void *desc_orig)
{
    usdm_mem_unit *addr_unit;
    uint64_t *pSrc1, *pSrc2, *pComp;
    uint64_t *pSrc1_usdm, *pSrc2_usdm, *pDest_usdm, *pComp_usdm;
    uint32_t src1_len, src2_len, dest_len;
    int is_aecs;
    uint8_t ts = 0;
    uint64_t *desc = (uint64_t *)desc_orig;

    addr_unit = (usdm_mem_unit *)desc_remapping;
    pSrc1_usdm = addr_unit->src1_virt;
    pSrc2_usdm = addr_unit->src2_virt;
    pDest_usdm = addr_unit->dest_virt;
    pComp_usdm = addr_unit->comp_virt;
    pSrc1 = (uint64_t *)desc[SRC1_ADDR_FIELD];
    pSrc2 = (uint64_t *)desc[SRC2_ADDR_FIELD];
    pComp = (uint64_t *)desc[COMP_ADDR_FIELD];
    src1_len = (uint32_t)(desc[SRC1_LEN_FIELD] & 0xFFFFFFFF);
    src2_len = (uint32_t)((desc[SRC2_LEN_FIELD] >> 32) & 0xFFFFFFFF);
    dest_len = (uint32_t)(desc[DEST_LEN_FIELD] & 0xFFFFFFFF);
    const uint32_t comp_len = 64;

    if (src1_len > USDM_PRE_ALLOCATION || src2_len > USDM_PRE_ALLOCATION || dest_len > USDM_PRE_ALLOCATION)
    {
        DIAG("Usdm memory copy overflow, src1 size 0x%x, src2 size 0x%x, dest size 0x%x\n",
                src1_len, src2_len, dest_len);
        return -1;
    }

    if (src1_len > 0)
    {
        memcpy((uint8_t *)pSrc1_usdm, (uint8_t *)pSrc1, src1_len);
        desc[SRC1_ADDR_FIELD] = (uint64_t)pSrc1_usdm;
    }

    if (src2_len > 0)
    {
        is_aecs = has_aecs_flag(desc, &ts);
        if (is_aecs == 1)
        {
            if ((src2_len << 1) > USDM_PRE_ALLOCATION)
            {
                DIAG("Usdm memory copy overflow for AECS, src2 size 0x%x, src2 hp size 0x%x\n",
                        (src2_len << 1), USDM_PRE_ALLOCATION);
                return -1;
            }
            if (ts == 0)
            {
                memcpy((uint8_t *)pSrc2_usdm, (uint8_t *)pSrc2, src2_len);
            }
            else
            {
                memcpy((uint8_t *)pSrc2_usdm + src2_len, (uint8_t *)pSrc2 + src2_len, src2_len);
            }
        }
        else
        {
            memcpy((uint8_t *)pSrc2_usdm, (uint8_t *)pSrc2, src2_len);
        }
        desc[SRC2_ADDR_FIELD] = (uint64_t)pSrc2_usdm;
    }

    if (dest_len > 0)
    {
        desc[DEST_ADDR_FIELD] = (uint64_t)pDest_usdm;
    }

    memcpy((uint8_t *)pComp_usdm, (uint8_t *)pComp, comp_len);
    desc[COMP_ADDR_FIELD] = (uint64_t)pComp_usdm;

    refill_work_descriptor(addr_unit, (void *)desc);

    return 0;
}

inline void post_copy(void *desc_remapping, void *desc_orig)
{
    usdm_mem_unit *addr_unit;
    uint64_t *pSrc2, *pDest, *pComp;
    uint64_t *pSrc2_usdm, *pDest_usdm, *pComp_usdm;
    uint32_t src2_len, output_size;
    int is_aecs;
    uint8_t ts = 0;
    uint64_t *desc = (uint64_t *)desc_orig;

    addr_unit = (usdm_mem_unit *)desc_remapping;
    pSrc2_usdm = addr_unit->src2_virt;
    pDest_usdm = addr_unit->dest_virt;
    pComp_usdm = addr_unit->comp_virt;
    pSrc2 = (uint64_t *)desc[SRC2_ADDR_FIELD];
    pDest = (uint64_t *)desc[DEST_ADDR_FIELD];
    pComp = (uint64_t *)desc[COMP_ADDR_FIELD];
    src2_len = (uint32_t)((desc[SRC2_LEN_FIELD] >> 32) & 0xFFFFFFFF);
    output_size = (uint32_t)(pComp_usdm[COMP_OUTPUT_SIZE_FIELD] & 0xFFFFFFFF);
    const uint32_t comp_len = 64;

    // result write back
    memcpy((uint8_t *)pComp, (uint8_t *)pComp_usdm, comp_len);
    if (output_size > 0)
    {
        memcpy((uint8_t *)pDest, (uint8_t *)pDest_usdm, output_size);
    }

    if (src2_len > 0)
    {
        is_aecs = has_aecs_flag(desc, &ts);
        if (is_aecs == 1)
        {
            if (ts == 0)
            {
                memcpy((uint8_t *)pSrc2 + src2_len, ((uint8_t *)pSrc2_usdm + src2_len), src2_len);
            }
            else
            {
                memcpy((uint8_t *)pSrc2, ((uint8_t *)pSrc2_usdm), src2_len);
            }
        }
    }
}

inline uint64_t *get_comp_virt(void *desc_remapping)
{
    usdm_mem_unit *addr_unit = (usdm_mem_unit *)desc_remapping;
    return addr_unit->comp_virt;
}

}
#endif
