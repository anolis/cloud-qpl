/*******************************************************************************
 * Copyright (C) 2022 Intel Corporation
 *
 * SPDX-License-Identifier: MIT
 ******************************************************************************/

#ifndef QPL_CHECK_IOMMU_HPP
#define QPL_CHECK_IOMMU_HPP

#include <unistd.h>
#include <fcntl.h>
#include <errno.h>
#include <string.h>
#include "hw_devices.h"

static inline bool check_iommu()
{
    int fd = 0;
    const char string_s[] = "intel_iommu=on,sm_on";
    char str_cmdline[4096];

    fd = open("/proc/cmdline", O_RDONLY);
    if (fd < 0)
    {
        DIAG("failed to open /proc/cmdline, %s\n", strerror(errno));
        return false;
    }
    if (read(fd, str_cmdline, 4096) < 0)
    {
        DIAG("failed to read /proc/cmdline, %s\n", strerror(errno));
        return false;
    }
    if (NULL != strstr(str_cmdline, string_s))
    {
        return true;
    }
    return false;
}

inline bool has_svm()
{
    static bool flag = check_iommu();
    return flag;
}

#endif //QPL_DESCRIPTOR_PROCESSING_HPP