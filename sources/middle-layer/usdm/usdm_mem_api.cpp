/*******************************************************************************
 * Copyright (C) 2022 Intel Corporation
 *
 * SPDX-License-Identifier: MIT
 ******************************************************************************/

/*
 *  Intel® Query Processing Library (Intel® QPL)
 *  Job API (public C API)
 */

#ifdef __cplusplus
extern "C"
{
#endif
#include <stdint.h>
#include <sys/types.h>
#include <cstdio>
#include "qae_mem_utils.h"
#include "qae_mem.h"
#include "usdm/usdm_mem_api.h"
#include "util/check_iommu.hpp"


int init_usdm_mem(void *usdm_mem)
{
    usdm_mem_unit *addr_unit = (usdm_mem_unit *)usdm_mem;

    if (has_svm())
    {
        DIAG("=====USDM: The system has iommu, skip usdm initialization\n");
        return 0;
    }

    create_qae_mem();

    /* SRC1 usdm initialization */
    addr_unit->src1_virt = (uint64_t *)qaeMemAllocNUMA(USDM_PRE_ALLOCATION, 0, 32);
    if (addr_unit->src1_virt == NULL)
    {
        DIAG("The USDM fails to allocate SRC1, len %d\n",
                USDM_PRE_ALLOCATION);
        goto err;
    }
    addr_unit->src1_phys = (uint64_t)qaeVirtToPhysNUMA(addr_unit->src1_virt);
    addr_unit->src1_len = USDM_PRE_ALLOCATION;

    /* SRC2 usdm initialization */
    addr_unit->src2_virt = (uint64_t *)qaeMemAllocNUMA(USDM_PRE_ALLOCATION, 0, 32);
    if (addr_unit->src2_virt == NULL)
    {
        DIAG("The USDM fails to allocate SRCs, len %d\n",
                USDM_PRE_ALLOCATION);
        qaeMemFreeNUMA((void **)&addr_unit->src1_virt);
        goto err;
    }
    addr_unit->src2_phys = (uint64_t)qaeVirtToPhysNUMA(addr_unit->src2_virt);
    addr_unit->src2_len = USDM_PRE_ALLOCATION;

    /* DEST usdm initialization */
    addr_unit->dest_virt = (uint64_t *)qaeMemAllocNUMA(USDM_PRE_ALLOCATION, 0, 32);
    if (addr_unit->dest_virt == NULL)
    {
        DIAG("The USDM fails to allocate DEST, len %d\n",
                USDM_PRE_ALLOCATION);
        qaeMemFreeNUMA((void **)&addr_unit->src1_virt);
        qaeMemFreeNUMA((void **)&addr_unit->src2_virt);
        goto err;
    }
    addr_unit->dest_phys = (uint64_t)qaeVirtToPhysNUMA(addr_unit->dest_virt);
    addr_unit->dest_len = USDM_PRE_ALLOCATION;

    /* COMP usdm initialization */
    addr_unit->comp_virt = (uint64_t *)qaeMemAllocNUMA(64, 0, 64);
    if (addr_unit->comp_virt == NULL)
    {
        DIAG("The USDM fails to allocate COMP, len %d\n", 64);
        qaeMemFreeNUMA((void **)&addr_unit->src1_virt);
        qaeMemFreeNUMA((void **)&addr_unit->src2_virt);
        qaeMemFreeNUMA((void **)&addr_unit->dest_virt);
        goto err;
    }
    addr_unit->comp_phys = (uint64_t)qaeVirtToPhysNUMA(addr_unit->comp_virt);
    addr_unit->comp_len = 64;

    return 0;

err:
    return -1;
}

void deinit_usdm_mem(void *usdm_mem)
{
    if (!usdm_mem)
    {
        DIAG("The usdm_mem_ptr is NULL\n");
        return;
    }

    if (has_svm())
    {
        DIAG("=====USDM: The system has iommu, skip usdm de-initialization\n");
        return;
    }

    if (!is_usdm_mem_initialized(usdm_mem))
    {
        return;
    }

    usdm_mem_unit *addr_unit = (usdm_mem_unit *)usdm_mem;
    qaeMemFreeNUMA((void **)&addr_unit->src1_virt);
    qaeMemFreeNUMA((void **)&addr_unit->src2_virt);
    qaeMemFreeNUMA((void **)&addr_unit->dest_virt);
    qaeMemFreeNUMA((void **)&addr_unit->comp_virt);
     if(NULL != addr_unit->src1_virt ||
        NULL != addr_unit->src2_virt ||
        NULL != addr_unit->dest_virt ||
        NULL != addr_unit->comp_virt)
    {
        /* If qaeMemFreeNUMA(void** ptr) successfully, the *ptr should be NULL.
        */
        DIAG("Qae Mem Free Error!\n");
    }
    addr_unit->src1_virt = NULL;
    addr_unit->src1_len = 0;
    addr_unit->src2_virt = NULL;
    addr_unit->src2_len = 0;
    addr_unit->dest_virt = NULL;
    addr_unit->dest_len = 0;
    addr_unit->comp_virt = NULL;
    addr_unit->comp_len = 0;
}

void create_qae_mem()
{
    static bool loaded = false;
    if (!has_svm() && !loaded)
    {
        DIAG("=====USDM init====\n");
        qaeMemInit();
        loaded = true;
    }
}

void destroy_qae_mem()
{
    if (!has_svm())
    {
        DIAG("=== USDM Destroy ===\n");
        qaeMemDestroy();
    }
}

bool is_usdm_mem_initialized(void *usdm_mem)
{
    /*
        * The check is assumed that the descriptor members(src1_virt/src2_virt/comp_virt/dest_virt)
        * length are not initialization correctly, we need a better way to check
        * this to avoid memory leak, since the lengths maybe be set correctly but
        * the memory has been allocated.
        */
    usdm_mem_unit *addr_unit = (usdm_mem_unit *)usdm_mem;
    if (NULL != addr_unit->src1_virt &&
        NULL != addr_unit->src2_virt &&
        NULL != addr_unit->dest_virt &&
        NULL != addr_unit->comp_virt &&
        USDM_PRE_ALLOCATION ==  addr_unit->src1_len &&
        USDM_PRE_ALLOCATION ==  addr_unit->src2_len &&
        USDM_PRE_ALLOCATION ==  addr_unit->dest_len &&
        64 ==  addr_unit->comp_len)
    {
        return true;
    }
    return false;
}

#ifdef __cplusplus
}
#endif
