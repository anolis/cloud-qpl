/*******************************************************************************
 * Copyright (C) 2022 Intel Corporation
 *
 * SPDX-License-Identifier: MIT
 ******************************************************************************/

#ifndef USDM_MEM_API_H_
#define USDM_MEM_API_H_

#ifdef __cplusplus
extern "C"
{
#endif

#define COMP_ADDR_FIELD (1)
#define SRC1_ADDR_FIELD (2)
#define DEST_ADDR_FIELD (3)
#define SRC2_ADDR_FIELD (5)
#define SRC1_LEN_FIELD (4)
#define SRC2_LEN_FIELD (6)
#define DEST_LEN_FIELD (6)

#define COMP_OUTPUT_SIZE_FIELD (3)

/* The pre-allocation memory size should be IAA max tranfer size ?
 * the 5K means USDM reserved fields so that the USDM_PRE_ALLOCATION
 * is 2M aligned to use 2M huge page
 * */
#define USDM_PRE_ALLOCATION ((2 * 1024 * 1024) - (5 * 1024))

/**
 * @brief Structure that contains the virtual address information of usdm.
 * Note: The mem of structure is allocated in QPL-inner part, not allocated by user.
 */
typedef struct
{
    uint64_t *src1_virt;
    uint64_t *src2_virt;
    uint64_t *dest_virt;
    uint64_t *comp_virt;
    uint64_t src1_phys; // Shares the same allocated memory with src1_virt.
    uint64_t src2_phys;
    uint64_t comp_phys;
    uint64_t dest_phys;
    uint64_t src1_len;
    uint64_t src2_len;
    uint64_t dest_len;
    uint64_t comp_len;
} usdm_mem_unit;

/**
 * @brief Usdm module initial API
 * @param usdm_mem_ptr: Points to an usdm_mem_unit struct whose mem is pre-allocated.
 */
int init_usdm_mem(void *usdm_mem);

/**
 * @brief Usdm module deinitial API
 * @param usdm_mem_ptr: Points to an usdm_mem_unit struct to be freed.
 */
void deinit_usdm_mem(void *usdm_mem);

/**
 * @brief destroy qae mem api.
 */
void destroy_qae_mem();

/**
 * @brief initialize qae mem api.
 */
void create_qae_mem();

/**
 * @brief Check if the descriptor remapping has been initialized.
 * @param usdm_mem_ptr: Points to an usdm_mem_unit.
 */
bool is_usdm_mem_initialized(void *usdm_mem);

#ifdef __cplusplus
}
#endif

#endif
